﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
//push test 2
namespace GitUndEmailTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Nachricht eingeben: ");
            string nachricht = Console.ReadLine();

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress("test@test.at"); //Absender 

            mail.To.Add("knibbern@qualifizierung.at"); //Empfänger

            mail.IsBodyHtml = true;

            mail.Subject = "Ihre Testnachricht";
            mail.Body = nachricht;

            SmtpClient client = new SmtpClient("SRV009.edu.local", 25); //ZUGANG ÜBER EDU NETZ!!!!
            //!!! nicht SSL im BBRZ verwenden !!!

            try
            {
                //KEIN Passwort nötig
                //client.Credentials = new System.Net.NetworkCredential("","");
                client.Credentials = new NetworkCredential("palkmic0@qualifizierung.at","");
                //var credentials = new NetworkCredential();

                //client.EnableSsl = true;

                client.Send(mail); //Senden 
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if(ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                }
            }
            Console.ReadKey();
        }
    }
}
